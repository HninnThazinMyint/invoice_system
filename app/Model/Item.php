<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class Item extends Model
{
	protected $fillable = ['id','item_name','price','qty','discount','invoice_id'];
	public function invoice()
    {
        return $this->belongsTo('App\Model\Incoice','invoice_id');
    }

}