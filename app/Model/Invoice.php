<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class Invoice extends Model
{
	protected $fillable = ['id','invoice_name'];
	public function item()
    {
        return $this->hasMany('App\Model\Item');
    }

}