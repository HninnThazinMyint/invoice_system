@extends('master')

@section('content')
<div class="col-md-12">
	<h1>Invoice System</h1>
	<div id="row">
		<label>Invoice Name</label>
		<input type="text" class="form-control invoice_name" style="width:300px;">
	</div>
	<div id="row" style="padding-top: 30px;">
		<table class="table table-bordered main_table">
			<thead>
				<tr>
					<th>id</th>
					<th>Item Name</th>
					<th>Qty</th>
					<th>Price</th>
					<th>Total</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="t_body">
				<input type="hidden" name="_token"  id="ctr_token" value="<?php echo csrf_token() ?>">
				<tr class="tr_clone">
					<td class="item_id" >1</td>
					<td><input type="text" class="form-control item_name"></td>
					<td><input type="number" class="form-control item_qty"></td>
					<td><input type="number" class="form-control item_price"></td>
					<td><input type="number" class="form-control item_total"></td>
					<td><button class="btn btn-primary add_row">Add</button>
						<button class="btn btn-danger delete_row">Remove</button></td>
				</tr>
			</tbody>
			<tfoot class="t_foot">
				<tr>
					<td colspan="4" style="text-align: right;">Total</td>
					<td><input type="number" class="form-control item_subtotal"></td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: right;">Discount</td>
					<td><input type="number" class="form-control item_discount"></td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: right;">Grand Total</td>
					<td><input type="number" class="form-control item_grandtotal"></td>
				</tr>
			</tfoot>
		</table>
		<button class="btn btn-success save" style="margin-left: 80%;"> Save</button>
	</div>
</div>
<script type="text/javascript">
	var j=2;	
	var subtotal=0;
	$('#t_body').delegate('.item_price','keyup',function(){
		var tr=$(this).closest('.tr_clone');
		var qty=tr.find('.item_qty').val();
		var price=tr.find('.item_price').val();
		var total=qty*price;
		tr.find('.item_total').val(total);
	});
	$('#t_body').delegate('.delete_row','click',function(){
		alert('delete');
		var del_row=$(this).closest('.tr_clone');
		del_row.remove();
		refresh_Table();
	});	

	$('.item_subtotal').click(function(){		
			$('.item_total').each(function(){
		    subtotal +=parseInt($(this).val());
			$('.item_subtotal').val(subtotal);
		});
	});

	$('.t_foot').delegate('.item_discount','keyup',function(){
		var discount= $('.item_discount').val();
		var sub_total=$('.item_subtotal').val();
		var grandtotal=sub_total *(1-(discount/100));
		$('.item_grandtotal').val(grandtotal);
	});

	$('#t_body').delegate('.add_row','click',function(){
		var tr=$(this).closest('.tr_clone');
		var new_row=tr.clone();	
		new_row.find('.item_id').html(j)
		new_row.find('.item_id').html(j);
		new_row.find('.item_name').val('');
		new_row.find('.item_qty').val('');
		new_row.find('.item_price').val('');
		new_row.find('.item_total').val('');	
		$('#t_body').append(new_row);
		j++;	
		refresh_Table();
	});
	function refresh_Table(){
		var i=1;
		$('.main_table >tbody>tr').each(function(){
			// var item_id=$(this).find('.item_id').text();
			var item_name=$(this).find('.item_name').val();
			var item_qty=$(this).find('.item_qty').val();
			var item_price=$(this).find('.item_price').val();
			var item_total=$(this).find('.item_total').val();
			$(this).find('.item_id').html(i);
			$(this).find('.item_name').val(item_name);
			$(this).find('.item_qty').val(item_qty);
			$(this).find('.item_price').val(item_price);
			$(this).find('.item_total').val(item_total);
			i++;
		});
	}	
	$('.save').click(function(){
		items = [];
		invoice=[];
		var order=1;
		$('.tr_clone').each(function(){
			arr={
				name : $(this).find('.item_name').val(),
				qty : $(this).find('.item_qty').val(),
				price :$(this).find('.item_price').val(),
			};
			items.push(arr);
		});
		jQuery.ajax({
			url : "http://localhost/invoice_system/public/create",
			type : "POST",
			data : {
					item : items,
					_token : $('#ctr_token').val() 
				    },
			dataType: "json",	   
		});
	});
</script>



@endsection