<html>
    <head>
        <title>@yield('Invoice System')</title>
         <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
         <script src="{{ asset('js/jquery.js') }}"></script>
         <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
           @yield('extra-css')
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>